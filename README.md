Individual Deals HTML:
<div ng-repeat="deal in dealsList.deals" ng-click="dealsList.toggleDetail(deal)" data-toggle="modal" data-target="#myModal" class="fav-deal col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 ng-scope">
	<div class="row">
		<div class="col-md-12 col-xs-12 inner">
			<div class="col-md-3 col-xs-3 business-image">
				<img ng-src="images/amerks.jpg" src="images/amerks.jpg">
			</div>
			<div class="col-md-6 col-xs-6 information">
				<a class="company ng-binding">Rochester Amerks</a>
				<p class="ng-binding">Free Tickets To Wednesday Night's Game!!</p>
				<div class="more">
					<div class="amount">
						<div class="number ng-binding">864</div>
						<div class="type">left</div>
					</div>
					<button class="share">Share</button>
				</div>
			</div>
			<div class="col-md-3 col-xs-3 ratings">
				<div class="buttons">
					<div class="like_btn">
						<button>
							<img src="images/thumbs.png">
							<p>Like</p>
						</button>
					</div>
					<div class="hide_btn">
						<button>
							<img src="images/thumbs_down.png">
							<p>Hide</p>
						</button>
					</div>
				</div>
			</div>
			<div class="social">
				<span class="ng-binding">Followed by Buzz Aldrin and 36 friends</span>
			</div>
		</div>
	</div>
	<div class="row testClass hide">
		<div class="col-md-12 col-xs-12 full-desc ng-binding">
			Fave this deal and get up to 4 free tickets to see the Rochester Americans face the Utica Comets on Wednesday November 3rd. Your tickets will be held at the Will Call line in the Blue Cross on game day. Hurry, tickets are limited!!
		</div>
	</div>
	<ul class="share hide">		
		<li class="btn profile">
			<button ng-click="viewBusinessProfile(deal)">Profile</button>
		</li>
		<li class="btn notify">
			<button>Notify Me</button>
		</li>
		<li class="btn share">
			<button>Share</button>
		</li>
		<li class="btn hide">
			<button>Hide</button>
		</li>		
	</ul>
</div>


Individual Deals Popup (Modal using Bootstrap):
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-right: 15px;">
	  <div class="modal-dialog" role="document">
	  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	    <div class="modal-content">
	    	<div class="bg_image"><img ng-src="images/amerks.jpg" src="images/amerks.jpg"></div>
		    <div class="fav-deal col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner">
							<div class="col-md-3 col-xs-3 business-image">
								<img ng-src="images/amerks.jpg" src="images/amerks.jpg" data-pin-nopin="true">
								<div class="amount">
									864 left
								</div>
							</div>
							<div class="col-md-9 col-xs-9 information">
								<div class="fave">
									<button>Fave Us!</button>
									<span>(Follow)</span>
								</div>
								<a class="company ng-binding">Rochester Amerks</a>
								<p class="ng-binding">Free Tickets To Wednesday Night's Game!!</p><div class="more">
								<div class="col-md-12 col-xs-12 full-desc ng-binding">
									Fave this deal and get up to 4 free tickets to see the Rochester Americans face the Utica Comets on Wednesday November 3rd. Your tickets will be held at the Will Call line in the Blue Cross on game day. Hurry, tickets are limited!!
									<a href="#profile">Visit Business Profile</a>
								</div>
								<ul class="buttons">
									<li class="redeem">
										<button>Redeem</button>
									</li>
									<li class="share">
										<button>Share</button>
									</li>									
								</ul>								
							</div>
						</div>
					</div>
				</div>
	    </div>
	  </div>
	</div>
</div>

<!-- Forgot Password -->
<div class="container deals">
	<div class="row ng-scope login forgot" style="margin: 20px 10px 0 10px;">
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-4 col-lg-4">
			<div class="well well-sm">
				We've received your request - if your email address was found in our system,
				we will send you a link to reset your password.
			</div>
		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<form class="ng-pristine ng-valid ng-valid-email">
				<div class="row" style="margin-top: 30px;">
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						<div class="form-group">
							<input ng-model="forgotPassword.email" type="email" class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-email" id="inputEmail" placeholder="your@email.com">
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<button class="btn btn-primary col-xs-12" ng-disabled="forgotPassword.email == &#39;&#39;" disabled="disabled">
							Get Password
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Registeration form -->
<div class="container deals">
	<div class="row login register ng-scope" style="margin: 20px 10px 0 10px;">
		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-offset-3 col-lg-6">
			<div class="well well-sm">
				Registering with Favular will net you personalized alerts from your favorite businesses! Already have account?
				<a ui-sref="login" href="http://localhost/favular/#/account/login">Log in here</a>.<br>
			</div>
		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<form class="ng-valid ng-valid-email ng-dirty ng-valid-number ng-submitted" style="">
				<div class="row" ng-show="register.error != null" style="">
					<div class="col-xs-12">
						<div class="panel panel-danger">
							<div class="panel-heading">Oops!</div>
							<div class="panel-body ng-binding">Please note that all fields on this form are required.</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputFirstName">First Name:</label>
							<input ng-model="register.firstName" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" placeholder="John" id="inputFirstName">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputLastName">Last Name:</label>
							<input ng-model="register.lastName" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="inputLastName" placeholder="Smith">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputDateOfBirth">Birthday:</label>
							<input ng-model="register.dateOfBirth" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="inputDateOfBirth" placeholder="mm/dd/yyyy">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputZipCode">Zip Code:</label>
							<input ng-model="register.zipCode" type="number" class="form-control ng-untouched ng-valid ng-empty ng-dirty ng-valid-number" id="inputZipCode" placeholder="xxxxx" style="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="inputMobilePhone">Mobile Phone:</label>
							<input ng-model="register.mobilePhone" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="inputMobilePhone" placeholder="(xxx) xxx-xxxx">
						</div>
					</div>
				</div>
				<hr>
				<div class="row" style="margin-top: 30px;">
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="inputEmail">Email</label>
							<input ng-model="register.email" type="email" class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-email" id="inputEmail" placeholder="your@email.com">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="inputConfirmEmail">Confirm email</label>
							<input ng-model="register.confirmEmail" type="email" class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-email" id="inputEmail" placeholder="your@email.com">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputPassword">Password</label>
							<input ng-model="register.password" type="password" class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="inputPassword" placeholder="Password" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="inputConfirmPassword">Confirm password</label>
							<input ng-model="register.confirmPassword" type="password" class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="inputPassword" placeholder="Password" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
						</div>
					</div>
				</div>
				<div class="row submit" style="margin-top: 30px; text-align: center;">
					<button ng-click="register.registerUser()" class="btn btn-primary">
						Register
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Options Section -->
<div class="container deals">
	<div class="v-options-container-outer">
    <div class="v-options-container-inner">        
        <!-- ngIf: options.isUserLoggedIn -->
        <!-- ngIf: options.isUserLoggedIn -->
        <!-- ngIf: options.isUserLoggedIn -->
        <!-- ngIf: options.isUserLoggedIn -->
        <!-- ngIf: !options.isUserLoggedIn -->
        <div ui-sref="login" class="row row-4 ng-scope" ng-if="!options.isUserLoggedIn" href="#/account/login">
            <div class="row row-5">
                <div class="options-row-caption" id="profile"><span class="profile-image"><img ng-src="images/profile.png" src="images/profile.png"></span>John Smith <a href="#" class="gear"><img ng-src="images/gear.png" src="images/gear.png"></a></div>
            </div>
            <div class="options-row-caption" id="login">Log In</div>
        </div><!-- end ngIf: !options.isUserLoggedIn --><!-- ngIf: options.isUserLoggedIn -->
        <div ui-sref="search" class="row row-5" href="#/search">
            <div class="options-row-caption">Search</div>
        </div>
        <!-- If Logged In  -->
        <div class="options-row-caption" id="logout">Log Out</div>
    </div>
	</div>
</div>

<!-- Search Section -->
<div class="container deals">
	<div class="ng-scope">
    <div class="search_outer">
      <div class="row" >
          <div class="type" id="retail">Retail</div>
      </div>
      <div class="row">
          <div class="type" id="travel">Travel</div>
      </div>
      <div class="row">
          <div class="type" id="services">service</div>
      </div>
      <div class="row">
          <div class="type" id="food">food & drink</div>
      </div>
			<div class="row">
	      <div class="type" id="entertainment">Entertainment</div>
	    </div>
	    <div class="row">
	      <div ui-sref="search-adv" class="col-xs-12" style="font-size: 0.8em; cursor: pointer; text-align: center; margin-bottom: 0.5em;" href="#/search-adv">
	            Search&nbsp;<div style="display: inline-block; cursor: pointer; width: 40%; height: 1em; background-color: #fff;"></div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
